#####################################
# Creator: Tomas Klas
# Contact: tomas.klas@seznam.cz
# Date: 2020-03-05
#####################################

from fastapi import FastAPI, status, Response
import psycopg2
import simplejson

app = FastAPI()

#DB = "localhost"
DB = "databaseservice"
fetching = 100


"""
Delete work will update DB with canceling the work and changing it to the False
"""


@app.put("/delete-work/{primkey}")
async def delete_work_for_hashID(primkey):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            UPDATE hashes
            SET solving = 'False'
            WHERE primkey = %s;    
            """,
            [primkey, ]
        )
        connection.commit()
        return {
            "Done"
        }

    except (Exception, psycopg2.Error) as error:
        return {
            "Could not connect to DB"
        }


"""
Not-found function will update DB with the new password len for the next cracking
"""


@app.put("/not-found/{primkey}/{passwdlen}")
async def passwd_not_found(primkey, passwdlen):
    try:
        passwdlen = int(passwdlen) + 2
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            UPDATE hashes
            SET solving = 'False',
                tested = %s
            WHERE primkey = %s;    
            """,
            [passwdlen, int(primkey)]
        )
        connection.commit()
        return {
            "Done"
        }

    except (Exception, psycopg2.Error) as error:
        return {
            "Could not connect to DB"
        }


"""
Not-found-dictionary function will update DB with the dictionary test to True
"""


@app.put("/not-found-dictionary/{primkey}/{passwdlen}")
async def passwd_not_found(primkey, passwdlen):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            UPDATE hashes
            SET solving = 'False',
                dictionary = 'True'
            WHERE primkey = %s;    
            """,
            [primkey, ]
        )
        connection.commit()
        return {
            "Done"
        }

    except (Exception, psycopg2.Error) as error:
        return {
            "Could not connect to DB"
        }

"""
Worker will ask for work and will get the hash and the type that he should solve.
Worker than will ask if he realy can take the wokr and will start solving.
"""

@app.get("/get-work")
async def find_work_for_worker():
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        # Select will chose all not cracked passwords
        # with the current minimal length 
        # ince with the md5 and the other time with sha1
        cursor.execute(
            """
            SELECT primkey,hash,type,tested 
            FROM hashes
            WHERE result = 'NULL'
            AND solving = 'False'
            AND type = 'md5'
            AND tested = (SELECT min(tested) FROM hashes WHERE solving = 'False')
            AND dictionary = 'True';
            """
        )

        result = cursor.fetchmany(fetching)
        
        if not result:
            cursor.execute(
                """
                SELECT primkey,hash,type,tested 
                FROM hashes
                WHERE result = 'NULL'
                AND solving = 'False'
                AND type = 'sha1'
                AND tested = (SELECT min(tested) FROM hashes WHERE solving = 'False')
                AND dictionary = 'True';
                """
            )
            result = cursor.fetchmany(fetching)

        if not result:
            cursor.execute(
                """
                SELECT primkey,hash,type,tested 
                FROM hashes
                WHERE result = 'NULL'
                AND solving = 'False'
                AND type = 'shadow'
                AND tested = (SELECT min(tested) FROM hashes WHERE solving = 'False')
                AND dictionary = 'True';
                """
            )
            result = cursor.fetchmany(fetching)


        hashes = []
        for row in result:
            hash = {
                'primkey': row[0],
                'hash': row[1],
                'type': row[2],
                'tested': row[3]
            }
            hashes.append(hash)
        return simplejson.dumps(hashes)

    except (Exception, psycopg2.Error) as error:    # No work have been found
        return {
            "Fail"
        }


@app.get("/get-work-dictionary")
async def find_work_for_worker_dictionary():
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        # Select will chose all not cracked passwords
        # with the current minimal length 
        # ince with the md5 and the other time with sha1

        # TODO make this in the cycle

        print("Testing md5")

        cursor.execute(
            """
            SELECT primkey,hash,type,tested 
            FROM hashes
            WHERE result = 'NULL'
            AND solving = 'False'
            AND type = 'md5'
            AND dictionary = 'False'
            """
        )
        
        result = cursor.fetchall()
        print(result)

        if not result:
            print("Result empty after md5")
            cursor.execute(
                """
                SELECT primkey,hash,type,tested 
                FROM hashes
                WHERE result = 'NULL'
                AND solving = 'False'
                AND type = 'sha1'
                AND dictionary = 'False'
                """
            )
            result = cursor.fetchall()

        print(result)

        if not result:
            print("Reslut empty after sha1")
            cursor.execute(
                """
                SELECT primkey,hash,type,tested 
                FROM hashes
                WHERE result = 'NULL'
                AND solving = 'False'
                AND type = 'shadow'
                AND dictionary = 'False'
                """
            )
            result = cursor.fetchall()
        


        hashes = []
        for row in result:
            hash = {
                'primkey': row[0],
                'hash': row[1],
                'type': row[2],
                'tested': row[3]
            }
            hashes.append(hash)
        
        return simplejson.dumps(hashes)

    except (Exception, psycopg2.Error) as error:    # No work have been found
        return {
            "Fail"
        }

"""
Worker will confirm if he can start on the fiven work in the previous get request.
If the work is assigned to another worker in that time it will return False and 
worker won't start working.
"""


@app.put("/start-work/{primkey}")
async def start_work_on_hash(primkey):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            SELECT solving 
            FROM hashes 
            WHERE primkey = %s
            """,
            [primkey, ]
        )
        line = cursor.fetchone()
        # print(line[0])
        # Work was probably given to another worker before
        # Worker will ask again after the False response on work.
        if line[0] != "False":
            return {
                "False"
            }
        else:
            # set hash as solving one cus one of the worker is 
            # working on it
            cursor.execute(
                """
                UPDATE hashes
                SET solving = 'True'
                WHERE primkey = %s;    
                """,
                [primkey, ]
            )
            connection.commit()
            return {
                "Start"
            }

    except (Exception, psycopg2.Error) as error:
        return {
            "Not such a result": "Failed"
        }


"""
After worker gets the work and he finishes it he will send put to update the DB.
He will send the hash and the result for the given hash.
"""


@app.put("/work-done/{primkey}/{result}")
async def update_result_for_hash(primkey, result):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        # TODO add check to hash it by myself before insert
        cursor = connection.cursor()
        # update hash with the result 
        cursor.execute(
            """
            UPDATE hashes
            SET solving = 'Done',
                result = %s
            WHERE primkey = %s;    
            """,
            [result.split('\n')[0], primkey, ]
        )
        connection.commit()
        return {
            "Great job worker"
        }

    except (Exception, psycopg2.Error) as error:
        return {
            "Not such a result": "Failed"
        }


"""
TODO
Working with Consul:
    Consul will send posts about the dead workers and API will update the DB.
    The DB should be update with one more collum with worker ID.
    Will have to check how to make the ID or how to identify them.
"""


"""
Check if the service is ready aka if it is connected to db or it can be connected.
And check if it is healthy by checking its api on /health.
"""


@app.get("/ready", status_code=200)
async def check_readiness_for_k8s(response: Response):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        return {"Health": "OK"}
    except (Exception, psycopg2.Error) as error:
        response.status_code = 400
        return {
            "Ready": "False"
        }


@app.get("/health", status_code=200)
async def check_healthiness_for_k8s():
    return {
        "Health": "OK"
    }


"""
On the root return just the info about this service.
"""


@app.get("/")
async def main():
    return {
        "This is private service, it serves data from DB to the workers"
    }
