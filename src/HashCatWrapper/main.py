import string, requests, json, subprocess, time, sys, getopt, time, urllib.request

# service = "http://localhost:8081/"
service = "http://privateservice:8081/"

# https://requests.readthedocs.io/en/master/user/quickstart/

dictF = "/opt/dict.txt"
hashesF = "/opt/Computationunit/hashes.txt"
crackedF = "/opt/Computationunit/cracked.txt"


def request_work(primkeyHashDict):
    # print("Requesting work ")
    # this is a check if the work isnt processed
    # somewhere else 
    for primkey in primkeyHashDict:
        url = service + "start-work/" + primkey
        approval = requests.put(url)
        # remove current hash from the list
        # and update DB with canceling work
        if approval == "Fail":
            #url = service + "delete-work/" + primkey
            #hashID.remove(primkey)
            del primkeyHashDict[primkey]
            #request.put(url)
    return True            


def start_work(primkeyHashDict, hashType, attack, passwdlen):
    # Create hashcracked.txt and write there hashes
    # that should be cracked in next step
    f = open(hashesF, "w+")
    for primkey in primkeyHashDict:    
        f.write(primkeyHashDict[primkey] + "\n")
    f.close()
    
    f = open(crackedF, "w+")
    f.close()
 
    # print("Start working with hashcat...")
  
    if hashType == "md5":
        hashType = "0"
    elif hashType == "sha1":
        hashType = "100"
    elif hashType == "shadow":
        hashType = "1800"
   
    if attack == "dictionary": 
        subprocess.run(["hashcat", "-m", hashType, "-o", crackedF, hashesF, dictF,"-O"])

    # TODO get the mask from the parameters

    if attack == "mask":
        # set the minimal len of password for not cracking multiple times with same hashes
        masklen = int(passwdlen) + 1
        if passwdlen == 0: 
            passwdlen = "--increment-min=1"
        else:
            passwdlen = "--increment-min=" + passwdlen
        maskfill = ""
     
     # creating mask len based on password length
        for i in range(0, masklen):
            maskfill = maskfill + "?1"
        mask = "abcdefghijklmnopqrstuvwxyz0123456789"
        # cracked password will be stored in cracked.txt with <HASH>:<password> format
        subprocess.run(["hashcat", "-m", hashType, "-a", "3", "-1", mask, "-i", passwdlen, "-o", crackedF, hashesF, maskfill,"-O"])

    # bruteforce attack is dumb mask attack, hashcat canceled support for this type 
    # of attack and started to force people to use just masks
    elif attack == "bruteforce":
        masklen = int(passwdlen) + 1
        if passwdlen == 0: 
            passwdlen = "--increment-min=1"
        else:
            passwdlen = "--increment-min=" + passwdlen
        maskfill = ""
        for i in range(0, masklen):
            maskfill = maskfill + "?1"
        mask = "abcdefghijklmnopqrstuvwxyz0123456789ABCEDFGHIJKLMNOPQRSTUVWXYZ%{}[]><,.!$^*#@"
        subprocess.run(["hashcat", "-m", hashType, "-a", "3", "-1", mask, "--increment", passwdlen, "-o", crackedF, hashesF, maskfill, "-O"])


"""
Worker will request privateservice for work based on the mode in which he is cracking the hashes.
Private service will return JSON and this function will parse it to dictionary primkey:hash.
It will call function to solve the hashes with HashCat and return solved hashes to the privateservice.
"""


def ask_for_work(attack):
    # print(attack)
    if attack == "dictionary":
        url = service + "get-work-dictionary/"
    else:
        url = service + "get-work/"
    try:
        # get hashes from privateservice
        response = urllib.request.urlopen(url)
        data = json.loads(response.read())
        data = data.replace('[','').replace(']','').replace('}, {', '}\n{')
        #print(data)        
    except (requests.exceptions.ConnectionError, IndexError):
        return 

    try:
        # parse the data and get password length
        # hashtype
        # and all of the hashes (list)
        primkeyHashDict = {}
        hashType = data.split('"type": "')[1].split('"')[0]
        passwdlen = data.split('d": ')[1].split('}')[0]
        for line in data.splitlines():
            primkey = line.split('"primkey": ')[1].split(',')[0]
            line = line.split('hash": "')[1].split('", "')[0]
            primkeyHashDict[primkey] = line
            print(primkeyHashDict[primkey])
            #hashID.append(line)

    except (TypeError, IndexError):
        print("Getting work failed.. returning...")
        return "empty"
    
    if request_work(primkeyHashDict):
        start_work(primkeyHashDict, hashType, attack, passwdlen)
        f = open(crackedF, "+r")
        crackedHashes = f.read()
        print(crackedHashes)
        solved = ""
        # check if the hash is in the cracked file 
        # if not return data to DB as not found 
        # and update len to +1 
        # ask the specific api if the attack was dictionary
        if attack == "dictionary":
            api = "not-found-dictionary/"
        else:
            api = "not-found/"

        for primkey in primkeyHashDict:
            i = primkeyHashDict[primkey]
            if i in crackedHashes:
                result = subprocess.check_output(['grep', i, crackedF]).decode().split(':')[1].split('\n')[0] 
                if result == '': 
                    result = subprocess.check_output(['grep', i, "/root/.hashcat/"]).decode().split(':')[1].split('\n')[0] 
                url = service + "work-done/" + str(primkey) + "/" + result
                print(url)
                requests.put(url)
            else:
                url = service + api + str(primkey) + "/" + str(passwdlen)
                print(url)
                requests.put(url)
 
    else:
        return "empty"


"""
This program takes an argument of the type of the attack it should run.
Than it runs this type of attack on the data that he gets from the private service.
Private service will provide hash and type and based on that it will solve the hash on the node.
"""


def main(argv):
    timeStart=int(time.time())
    attack = "dictionary"
    try:
        opts, args = getopt.getopt(argv,"hdmb")
    except getopt.GetoptError:
        print("python3 main.py [OPTIOM]")
        print("OPTIONS: ") 
        print("     -b = bruteforce")
        print("     -m = mask")
        print("     -d = dict")
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print("python3 main.py [OPTIOM]")
            print("OPTIONS: ") 
            print("     -b = bruteforce")
            print("     -m = mask")
            print("     -d = dict")
            sys.exit()
        elif opt == "-b":
            attack = "bruteforce"
        elif opt == "-m":
            attack = "mask"
        elif opt == "-d":
            attack = "dictionary"

    while True:
        print("Asking for work") 
        returned = ask_for_work(attack)
        time.sleep(2)

if __name__ == '__main__':
    main(sys.argv[1:])

