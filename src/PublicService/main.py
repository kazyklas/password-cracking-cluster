#####################################
# Creator: Tomas Klas
# Contact: tomas.klas@seznam.cz
# Date: 2020-03-05
#####################################

import os
import psycopg2
import simplejson
import random
from fastapi import FastAPI, Form, File, UploadFile, Response
from fastapi.responses import HTMLResponse

# TODO Try to monetize this in the diploma thesis

app = FastAPI()

DB = "databaseservice"
#DB = "localhost"
#DB = "172.17.0.1"


"""
Create_file is a function to get the file with hashes from user and update DB
with data that are in the file.
Also it will get the name of the hash for future cracking.
"""


@app.post("/files/")
async def insert_hashes_and_hashType_to_DB(files: UploadFile = File(...), hashtype: str = Form(...)):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        # Check the file if it is a supported format
        if os.path.splitext(files.filename)[1] != '.txt':
            return {
                "Database Fill": "Failed",
                "Wrong file format": "Not .txt"
            }
        # check the supported hashes in the current time 
        if hashtype != "md5" and hashtype != "sha1" and hashtype != "shadow":
             return {
                 "Database Fill": "Failed",
                 "Wrong hash type": "md5, sha1, shadow"
             }

        cursor.execute(
            """
            select max(primkey) from hashes;
            """
        )

        primkey = cursor.fetchone()
        if primkey == (None,):
            primkey = 1
        else:
            primkey = int(primkey[0])+1
        
        print(primkey)

        while True:
            line = files.file.readline().decode("UTF-8").rstrip()
            if line == '':
                break
            else:
                # set random passwd len ( 0, 2, 4 ) for more 
                # optimized cracking when we hame multiple workers
                randomnum = random.randrange(0, 6, 2)
                cursor.execute(
                    """
                    INSERT INTO 
                    hashes (primkey, hash, type, solving, result, tested, dictionary) 
                    VALUES (%s, %s, %s, %s, %s, %s, %s);
                    """,
                    [primkey, line, hashtype, 'False', 'NULL', randomnum, 'False']
                )
                primkey = primkey+1

        connection.commit()
        return {
            "Database Fill": "Success"
        }

    except (Exception, psycopg2.Error) as error:
        return {
            "Database Fill": "Failed"
        }


"""
This function will return the set of the hashes that are in the DB.
It will show ale the data that are in the DB.
Not solving which user added or another stuff like that.
"""


@app.get("/solved")
async def print_all_cracked_hashes(response: Response):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            SELECT * 
            FROM hashes
            WHERE solving = 'Done' 
            """
        )
        records = cursor.fetchall()
        hashes = []
        for row in records:
            hash = {
                'hash': row[1],
                'result': row[4]
            }
            hashes.append(hash)

        return simplejson.dumps(hashes)

    except (Exception, psycopg2.Error) as error:
        response.status_code = 500
        return {
            "Connect to DB": "Failed"
        }


# https://stackoverflow.com/questions/57343510/how-to-use-a-string-variable-in-psycopg2-select-query
"""
This function will take an argument from the curl with result.
For this result will try to find the hash in the DB.
"""


@app.get("/solved/result/{result}")
async def print_hash_for_result(result):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            SELECT hash 
            FROM hashes h
            WHERE h.result = %s;
            """,
            [result, ]
        )

        result = cursor.fetchone()
        if result == "":
            return {
                "Not found"
            }
        return simplejson.dumps(result)

    except (Exception, psycopg2.Error) as error:
        return {
            "Connection to DB": "Failed"
        }


"""
Function will take an argument with hash and will try to find result.
Will return a JSON that contains the result, null if not solved.
"""


@app.get("/solved/hash/{primkey}")
async def print_result_for_hash(primkey):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            SELECT result 
            FROM hashes
            WHERE primkey = %s;
            """,
            [primkey, ]
        )

        result = cursor.fetchone()

        return simplejson.dumps(result)

    except (Exception, psycopg2.Error) as error:
        return {
            "Not such a result": "Failed"
        }

@app.get("/all")
async def get_all_data_from_db():
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        cursor = connection.cursor()
        cursor.execute(
            """
            SELECT * FROM hashes;
            """
        )

        result = cursor.fetchall()

        return simplejson.dumps(result)

    except (Exception, psycopg2.Error) as error:
        return {
            "Not such a result": "Failed"
        }


"""
Check if the service is ready aka if it is connected to db or it can be connected.
And check if it is healthy by checking its api on /health.
"""


@app.get("/ready", status_code=200)
async def check_readiness_for_k8s(response: Response):
    try:
        connection = psycopg2.connect(database="cracking", user="postgres", password="password1", host=DB, port="5432")
        return {"Health": "OK"}
    except (Exception, psycopg2.Error) as error:
        response.status_code = 500
        return {
            "Ready": "False"
        }


@app.get("/health", status_code=200)
async def check_healthiness_for_k8s():
    return {
        "Health": "OK"
    }


@app.get("/")
async def main():
    content = """
    <style>
        body {
          height: 100%;
        }

        body {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-align: center;
            align-items: center;
            padding-top: 40px;
            padding-bottom: 40px;
            padding-left: 300px;
            background-color: #f5f5f5;
        } 
        
    </style>
    <html> 
        <head>
            <meta name="description" content="">
            <meta name="author" content="Kazyklas">
            <title>Password Recovery</title>
        </head>  

        <body>
            <form action="/files/" enctype="multipart/form-data" method="post">
            <input name="hashtype" type="text" placeholder="SHA1" required autofocus><p>
            <input name="files" type="file" multiple><p>
            <input type="submit">
            </form>
        </body>
    </html>

    """
    return HTMLResponse(content=content)


