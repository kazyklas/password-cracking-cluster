\changetocdepth {3}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\contentsline {chapter}{{\' U}vod}{1}{chapter*.8}%
\contentsline {chapter}{\chapternumberline {1}Kubernetes}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Pod}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}Control Plane}{6}{section.1.2}%
\contentsline {section}{\numberline {1.3}Komponenty nodu}{7}{section.1.3}%
\contentsline {subsection}{\numberline {1.3.1}Kubelet}{7}{subsection.1.3.1}%
\contentsline {subsection}{\numberline {1.3.2}Kube-Proxy}{7}{subsection.1.3.2}%
\contentsline {section}{\numberline {1.4}Mikroslužby}{8}{section.1.4}%
\contentsline {chapter}{\chapternumberline {2}Ansible}{11}{chapter.2}%
\contentsline {section}{\numberline {2.1}Control node}{12}{section.2.1}%
\contentsline {section}{\numberline {2.2}Managed node}{12}{section.2.2}%
\contentsline {section}{\numberline {2.3}Iventory}{12}{section.2.3}%
\contentsline {section}{\numberline {2.4}Modules}{12}{section.2.4}%
\contentsline {section}{\numberline {2.5}Tasks}{13}{section.2.5}%
\contentsline {section}{\numberline {2.6}Playbooks}{13}{section.2.6}%
\contentsline {chapter}{\chapternumberline {3}Docker}{15}{chapter.3}%
\contentsline {section}{\numberline {3.1}Kontejner vs. virtuální počítač}{15}{section.3.1}%
\contentsline {section}{\numberline {3.2}Stavební kameny Dockeru}{17}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Jmenné prostory}{17}{subsection.3.2.1}%
\contentsline {subsection}{\numberline {3.2.2}Kontrolní skupina}{18}{subsection.3.2.2}%
\contentsline {subsection}{\numberline {3.2.3}Docker daemon}{19}{subsection.3.2.3}%
\contentsline {subsection}{\numberline {3.2.4}Docker klient}{19}{subsection.3.2.4}%
\contentsline {subsection}{\numberline {3.2.5}Docker registr}{19}{subsection.3.2.5}%
\contentsline {subsection}{\numberline {3.2.6}Obrazy}{19}{subsection.3.2.6}%
\contentsline {section}{\numberline {3.3}Systémové kontejnery}{19}{section.3.3}%
\contentsline {chapter}{\chapternumberline {4}Hesla}{23}{chapter.4}%
\contentsline {section}{\numberline {4.1}Windows}{23}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Kerberos}{24}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}NTLM}{24}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Linux}{25}{section.4.2}%
\contentsline {section}{\numberline {4.3}Hašovací funkce}{26}{section.4.3}%
\contentsline {subsection}{\numberline {4.3.1}MD5}{28}{subsection.4.3.1}%
\contentsline {subsection}{\numberline {4.3.2}SHA-1}{28}{subsection.4.3.2}%
\contentsline {section}{\numberline {4.4}Útoky na hesla}{30}{section.4.4}%
\contentsline {section}{\numberline {4.5}Ochrany prolomení a autentizační faktory}{32}{section.4.5}%
\contentsline {section}{\numberline {4.6}Složitost hesel}{33}{section.4.6}%
\contentsline {section}{\numberline {4.7}Solení hesla}{34}{section.4.7}%
\contentsline {chapter}{\chapternumberline {5}Jiná řešení}{35}{chapter.5}%
\contentsline {section}{\numberline {5.1}Hashtopolis}{35}{section.5.1}%
\contentsline {section}{\numberline {5.2}CrackLord}{36}{section.5.2}%
\contentsline {section}{\numberline {5.3}Distributed Hash Cracker}{36}{section.5.3}%
\contentsline {section}{\numberline {5.4}Gocrack}{36}{section.5.4}%
\contentsline {chapter}{\chapternumberline {6}Vytvoření klastru na lámání hesel }{37}{chapter.6}%
\contentsline {section}{\numberline {6.1}Realizovaný klastr}{37}{section.6.1}%
\contentsline {section}{\numberline {6.2}Nastavení a nasazení klastru}{39}{section.6.2}%
\contentsline {section}{\numberline {6.3}Digital Ocean}{40}{section.6.3}%
\contentsline {chapter}{\chapternumberline {7}Návrh aplikace na lámání hesel}{41}{chapter.7}%
\contentsline {section}{\numberline {7.1}Databáze}{41}{section.7.1}%
\contentsline {section}{\numberline {7.2}Publicservice}{42}{section.7.2}%
\contentsline {section}{\numberline {7.3}Privateservice}{42}{section.7.3}%
\contentsline {section}{\numberline {7.4}Computationunit}{43}{section.7.4}%
\contentsline {section}{\numberline {7.5}Hashcat}{44}{section.7.5}%
\contentsline {subsection}{\numberline {7.5.1}OpenCL runtime}{44}{subsection.7.5.1}%
\contentsline {chapter}{\chapternumberline {8}Analýza hesel a výkonu klastru}{47}{chapter.8}%
\contentsline {section}{\numberline {8.1}Prolomení jednoho hesla}{47}{section.8.1}%
\contentsline {section}{\numberline {8.2}Prolomení množiny hesel podle instancí}{49}{section.8.2}%
\contentsline {section}{\numberline {8.3}Prolomení hesla podle velikosti}{51}{section.8.3}%
\contentsline {chapter}{\chapternumberline {9}Závěr}{53}{chapter.9}%
\contentsline {chapter}{Literatura}{55}{chapter*.9}%
\contentsline {appendix}{\chapternumberline {A}Seznam použitých zkratek}{59}{appendix.A}%
\contentsline {appendix}{\chapternumberline {B}Obsah přiloženého CD}{61}{appendix.B}%
